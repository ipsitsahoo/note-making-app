package com.example.root.note_maker;

/**
 * Created by Ipsit Sahoo on 10/6/17.
 * For testing
 */

public class Note {
    private int id;
    private String title;
    private String desc = "" ;

    public Note() {

    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() { return title;}

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() { return desc;}

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
