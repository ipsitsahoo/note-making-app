package com.example.root.note_maker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ipsit Sahoo on 10/6/17.
 * For testing
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    public DatabaseHandler(Context context) {
        super(context, DatabaseValues.DBNAME, null, DatabaseValues.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseValues.NOTES_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DatabaseValues.NOTES_TABLE_DROP);
        onCreate(db);
    }

    public void addNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues nvs = new ContentValues();
        nvs.put(DatabaseValues.NOTES_TITLE, note.getTitle());
        nvs.put(DatabaseValues.NOTES_DESC, note.getDesc());
        db.insert(DatabaseValues.TABLE, null, nvs);
        db.close();
    }

    public void updateNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues nvs = new ContentValues();
        nvs.put(DatabaseValues.NOTES_TITLE, note.getTitle());
        nvs.put(DatabaseValues.NOTES_DESC, note.getDesc());
        db.update(DatabaseValues.TABLE, nvs, DatabaseValues.NOTES_ID + " = ?", new String[]{String.valueOf(note.getId())});
        db.close();
    }

    public void deleteNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        String del_query = "DELETE FROM " + DatabaseValues.TABLE + " WHERE " + DatabaseValues.NOTES_ID + " = '" + note.getId() + "'";

        db.execSQL(del_query);
        db.close();
    }

    public List<Note> getAllNotes() {
        List<Note> nt = new ArrayList<Note>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + DatabaseValues.TABLE;
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {

                Note note = new Note();
                note.setId(Integer.parseInt(cursor.getString(0)));
                note.setTitle(cursor.getString(1));
                note.setDesc(cursor.getString(2));

                nt.add(note);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return nt;
    }
}

