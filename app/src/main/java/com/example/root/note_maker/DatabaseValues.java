package com.example.root.note_maker;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by root on 10/6/17.
 * For testing
 */

public class DatabaseValues {
    public static final int DATABASE_VERSION = 1;
    public static final String DBNAME = "fdb";
    public static final String TABLE = "notes";

    //Table Columns
    public static final String NOTES_ID = "id";
    public static final String NOTES_TITLE = "title";
    public static final String NOTES_DESC = "desc";

    //Create Table Queries
    public static final String NOTES_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE + " ( " + NOTES_ID + " INTEGER PRIMARY KEY, " + NOTES_TITLE + " TEXT, " + NOTES_DESC + " TEXT)";
    public static final String NOTES_TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE;
}