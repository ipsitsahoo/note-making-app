package com.example.root.note_maker;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button addNote;
    DatabaseHandler dbh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addNote = (Button) findViewById(R.id.addNote);
        addNote.setOnClickListener(this);
        dbh = new DatabaseHandler(this);
    }

    @Override
    public void onClick(View v) {
        Intent myIntent = new Intent(MainActivity.this);
        Bundle bundle = new Bundle();
        bundle.putString("source", "PressAddd");
        myIntent.putExtras(bundle);

        startActivity(myIntent);
    }
}
